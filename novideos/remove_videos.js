function main() {
    var replace_functions = [
        replace_jwplayer(),
        replace_ultimedia_digika(),
        replace_html5_video_tag(),
        replace_dmp_player(),
        replace_daily_motion(),
        replace_videojs()
    ]
    //recursive doc + iframe: document.getElementsByTagName("iframe")
    // https://developer.mozilla.org/fr/docs/Web/API/Document/getElementsByTagName
    for (var f of replace_functions) {
        f(document)
        setTimeout(f, 3000)
        setTimeout(f, 5000)
    }
}

function replace_jwplayer() {
    return function() {
        for (var node of document.getElementsByClassName("jwplayer")){
            _clean_node(node)
        }
    }
}

function _clean_node(video_node) {
    var info_node = document.createElement("span");
    info_node.innerHTML = "<span style='font-family: monospace; text-decoration: line-through;'>[VIDEO]</span>"
    video_node.parentNode.appendChild(info_node);
    video_node.remove()
}

function replace_ultimedia_digika() {
    return function() {
        var node = document.getElementById("ultimedia_wrapper")
        if (node) {
            _clean_node(node)
        }
    }
}

function replace_html5_video_tag() {
    return function() {
        for (var node of document.getElementsByTagName("video")){
            _clean_node(node)
        }
    }
}

function replace_dmp_player() {
    return function() {
        for (var node of document.getElementsByClassName("dmp_Player")){
            _clean_node(node)
        }
    }
}

function replace_dailymotion() {
    return function() {
        for (var node of document.getElementsByClassName("dailymotion-player-root")){
            _clean_node(node)
        }
    }
}

function replace_videojs() {
    // https://videojs.com/
    return function() {
        for (var node of document.getElementsByClassName("player-container")){
            _clean_node(node)
        }
        for (var node of document.getElementsByClassName("video-js")){
            _clean_node(node)
        }

    }
}


main()
