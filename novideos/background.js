var removalMode = true
var browserButton = browser.browserAction


function executeVideoRemoval(){
    if (removalMode) {
        browser.tabs.query(
            {currentWindow: true, active: true},
            _executeVideoRemovalOnActiveTab
        )
    }
}


function _executeVideoRemovalOnActiveTab(tabs) {
    const activeTab = tabs[0]
    browser.tabs.executeScript(
	activeTab.id,
	{file: 'remove_videos.js'}
    )
}


function toggleRemovalMode() {
    removalMode = !removalMode
    browserButton.setIcon({"path": _setButtonIcon(removalMode)})
    browserButton.setTitle({"title": _setButtonTitle(removalMode)})
    executeVideoRemoval()
}


function _setButtonIcon(condition) {
    const infix = (condition ? "disabled" : "enabled")
    return {
        48: "icons/logo-" + infix + ".svg",
        96: "icons/logo-" + infix + ".svg"
    }
}

function _setButtonTitle(condition) {
    return condition ? "Keep videos" : "Remove videos"
}


browserButton.onClicked.addListener(toggleRemovalMode)
browser.tabs.onUpdated.addListener(executeVideoRemoval)
browser.tabs.onActivated.addListener(executeVideoRemoval)
