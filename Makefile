archive: clean
	./archive.sh

clean:
	find . -name "*~" -delete
	rm -f icon*.png novideos.zip

iconpng:
	inkscape -w 128 -h 128 novideos/icons/logo.svg --export-png icon128.png
